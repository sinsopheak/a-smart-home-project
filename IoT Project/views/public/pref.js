var currency;
var siteData = [["US Dollar", " $ per kilowatt", "Change to Riel"], ["Khmer Riel", " Riel per kilowatt", "Change to US Dollar"]];
var cost;
var user;

// Display data
function setData(cur) {
    var index;
    if (cur == "USD") {
        index = 0;
    } else index = 1;
    document.getElementById('curr').textContent = siteData[index][0];
    document.getElementById('swCurr').textContent = siteData[index][2];
    var c;
    if (index == 0) {
        c = cost;
    } else {
        c = cost * 4000;
    }
    document.getElementById('cost').textContent = c + siteData[index][1];
}

function switchCurrency() {
    var url = '/pref/swCurr/' + currency;
    fetch(url, { method: 'POST' })
        .then(function (response) {
            if (response.ok) {
                var tmpCurr;
                if (currency == "USD") {
                    currency = "Riel";
                } else {
                    currency = "USD";
                }
                setData(currency);
                return true;
            }
            return false;
        })
        .catch(function (error) {
            console.log(error);
        });
}

$('#swCurr').click(function () { 
    switchCurrency();
});

// Get data
$.ajax({
    type: 'GET',
    url: '/pref/data',
    dataType: 'json',
    complete: function (data) {
        user = data['responseJSON']['user'];
        document.getElementById('dp').textContent = user['username'];
        var result = data['responseJSON']['data'];
        cost = result['usd_per_kw'];
        currency = result['currency'];
        setData(currency);
    }
});

