function StatRecord(day, watt, cost){
    this.day = day;
    this.watt = watt;
    this.cost = cost;
}
var isUSD = true;

// Daily usage
var arr_stat = [];
var arr = [['Last seven days', 'watt', 'cent']];

// Monthly usage
var month = [['Usage per month', 'watt', 'cent']];
var monthly = [];

function drawMaterial() {
    // Define the chart to be drawn.
    var data = google.visualization.arrayToDataTable(arr);

    var options = { title: 'Power usage (watt)' };

    // Instantiate and draw the chart.
    var chart = new google.charts.Bar(document.getElementById('chart_div'));
    chart.draw(data, options);
}

function drawMonth() {
    // Define the chart to be drawn.
    var data = google.visualization.arrayToDataTable(month);

    var options = { title: 'Power usage (watt)' };

    // Instantiate and draw the chart.
    var chart = new google.charts.Bar(document.getElementById('chart_month'));
    chart.draw(data, options);
}

// Get usage and logs
$.ajax({
    type: 'GET',
    url: '/pref/log',
    dataType: 'json',
    complete: function (data) {
        setLog(data);
        setStats(data);
        setMonth(data);
    }
});

// Display log events
function setLog(data){
    var text = "";
    var json = data['responseJSON']['message'];
    var domLog = document.getElementById('log');
    for (i = 0; i < json.length; i++) {
        var row = "<div class='row'> <div class='col-lg-6'>" + json[i]['date'] +"</div> <div class='col-lg-6'>Turned " + json[i]['event'] + "</div> </div>";
        text = text + "" + row;
    } 
    domLog.innerHTML = text;
}

// Display Monthly usage
function setMonth(data) {
    monthly = [];
    var json = data['responseJSON']['month'];
    if (!(data['responseJSON']['curr'])) {
        isUSD = false;
    }
    for (i = 0; i < json.length; i++) {
        var ym = json[i]['y'] + "-" + json[i]['m'];
        var record = new StatRecord(ym, json[i]['watt'], json[i]['cost']);
        monthly.push(record);
        var cost;

        if (isUSD) {
            cost = 100; //convert to USD cent
        } else {
            month[0][2] = "Riel";
            cost = 4000; //convert USD to Riel
        }
        month.push([record.day, record.watt, (record.cost * cost)]);
    }
    google.charts.load('current', { packages: ['corechart', 'bar'] });
    google.charts.setOnLoadCallback(drawMonth);
}

// Display daily usage
function setStats(data) {
    arr_stat = [];
    var json = data['responseJSON']['stat'];
    if (!(data['responseJSON']['curr'])) {
        isUSD = false;
    }
    for (i = 0; i < json.length; i++) {
        var record = new StatRecord(json[i]['date'], json[i]['watt'], json[i]['total']);
        arr_stat.push(record);
        var cost;
        
        if (isUSD) {
            cost = 100; //convert to USD cent
        } else {
            arr[0][2] = "Riel";
            cost = 4000; //convert USD to Riel
        }
        arr.push([record.day, record.watt, (record.cost * cost)]);
    }
    google.charts.load('current', { packages: ['corechart', 'bar'] });
    google.charts.setOnLoadCallback(drawMaterial);
}