var commandText = ["on", "off"];
var imgText = ["./img/light-bulb-on.png", "./img/light-bulb-off.png"];

/* ---- Ground floor lights ----- */
const glight = document.getElementById('glight');
const gbulb = document.getElementById('gbulb');
const gbstate = document.getElementById('gbstate');

/* ---- First floor lights ----- */
const flight = document.getElementById('flight');
const fbulb = document.getElementById('fbulb');
const fbstate = document.getElementById('fbstate');

/* ---- Fan ----- */
const gfan = document.getElementById('gfan');
const imgfan = document.getElementById('imgfan');
const fanstate = document.getElementById('fanstate');

/* ---- Ground floor lights ----- */
function c_glight(code) {
    if (code == 2) {
        if (gbstate.textContent == 'Off') {
            i = 0;
            glight.classList.add('columnActive');
        } else {
            i = 1;
            glight.classList.remove('columnActive');
        }
    } else {
        if (code == 1) {
            i = 0;
            glight.classList.add('columnActive');
        } else if (code == 0) {
            i = 1;
            glight.classList.remove('columnActive');
        }
    }
    gbulb.src = imgText[i];
    gbstate.textContent = commandText[i].charAt(0).toUpperCase() + commandText[i].substring(1);
}

/* ---- First floor lights ----- */
function c_flight(code) {
    if (code == 2) {
        if (fbstate.textContent == 'Off') {
            j = 0;
            flight.classList.add('columnActive');
        } else {
            j = 1;
            flight.classList.remove('columnActive');
        }
    } else {
        if (code == 1) {
            j = 0;
            flight.classList.add('columnActive');
        } else if (code == 0) {
            j = 1;
            flight.classList.remove('columnActive');
        }
    }
    fbulb.src = imgText[j];
    fbstate.textContent = commandText[j].charAt(0).toUpperCase() + commandText[j].substring(1);
}

/* ---- Fan ----- */
function c_fan(code) {
    if (code == 2) {
        if (fanstate.textContent == 'Off') {
            j = 0;
            gfan.classList.add('columnActive');
        } else {
            j = 1;
            gfan.classList.remove('columnActive');
        }
    } else {
        if (code == 1) {
            j = 0;
            gfan.classList.add('columnActive');
        } else if (code == 0) {
            j = 1;
            gfan.classList.remove('columnActive');
        }
    }
    fanstate.textContent = commandText[j].charAt(0).toUpperCase() + commandText[j].substring(1);
}

function control(command, index) {
    var urls = ['/light/g/', '/light/f/', '/fan/'];
    var i = index;
    var url = urls[i] + command;
    console.log(url);
    fetch(url, { method: 'POST' })
        .then(function (response) {
            if (response.ok) {
                switch (i) {
                    case 0:
                        c_glight(2);
                        break;
                    case 1:
                        c_flight(2);
                        break;
                    case 2:
                        c_fan(2);
                        break;
                }
                return true;
            } else {
               return false;
            }
        })
        .catch(function (error) {
            console.log(error);
        });
}

glight.addEventListener('click', function (e) {
    var i;
    if (gbstate.textContent == 'Off') {
        i = 0;
    } else {
        i = 1;
    }
    control(commandText[i], 0);
});

flight.addEventListener('click', function (e) {
    if (fbstate.textContent == 'Off') {
        j = 0;
    } else {
        j = 1;
    }
    control(commandText[j], 1);
});

gfan.addEventListener('click', function (e) {
    if (fanstate.textContent == 'Off') {
        j = 0;
    } else {
        j = 1;
    }
    control(commandText[j], 2);
});
