const path = require('path');
const express = require('express');
const exphbs = require('express-handlebars');
const mqtt = require('mqtt');
const mysql = require('mysql');
var schedule = require('node-schedule');
var app = express();
var dateFormat = require('dateformat');
var fs = require('fs');

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var session = require('express-session');

var favicon = require('serve-favicon');
app.use(favicon(path.join(__dirname, 'views', 'public', 'img', 'app-icon.ico')));

var client = mqtt.connect('http://127.0.0.1');
client.on('connect', () => {
  client.subscribe('light/status');
  console.log('connected to mqtt server');
});

const con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "db_smart_home"
});

var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
server.listen(3000);

//For keeping up-todate the state of each component (lights, fan)
io.on('connection', function (ioClient) {
  console.log('Client connected...');
  ioClient.on('join', function (data) {
    if (data == "status") { // Client asks for component states
      // client = mqtt.connect('http://127.0.0.1');
      // client.on('connect', () => {
      //   client.publish("light/status/request", "1");
      // });
      var gl = -1;
      var fan = -1;
      var fl = -1;
      con.query('SELECT gl, fan, fl FROM unit WHERE unID = "1"', (err, res) => {
        if (err) throw err;
        gl = res[0].gl;
        fan = res[0].fan;
        fl = res[0].fl;
        var value = gl + "" + fan + "" + fl;
        ioClient.emit('messages', value);
      })
    }
  });
  client.on('message', (topic, message) => { // Component state from sensor/board
    if (topic == 'light/status') {
          var msg = message.toString();
          if (msg == "0") {
            con.query('UPDATE unit SET gl = 0 WHERE unID = "1"', (err, result) => {
              if (err) throw err;
            })
          } else if (msg == "1") {
            con.query('UPDATE unit SET gl = 1 WHERE unID = "1"', (err, result) => {
              if (err) throw err;
            })
          }
          ioClient.emit('messages', msg);
    }
  });
});

var isTodayLogged = false;
var todayUsgID = -1;

// initialize body-parser to parse incoming parameters requests to req.body
app.use(bodyParser.urlencoded({ extended: true }));

// initialize cookie-parser to allow us access the cookies stored in the browser. 
app.use(cookieParser());

// initialize express-session to allow us track the logged-in user across sessions.
app.use(session({
  key: 'user_sid',
  secret: 'somerandonstuffs',
  resave: false,
  saveUninitialized: false,
  cookie: {
    expires: 600000
  }
}));

// This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
// This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
    res.clearCookie('user_sid');
  }
  next();
});

// middleware function to check for logged-in users
var sessionChecker = (req, res, next) => {
  if (req.session.user && req.cookies.user_sid) {
    res.redirect('/home');
  } else {
    next();
  }
};

// serve files from the public directory
app.use(express.static(__dirname + '/views/public/'));

app.get('/', sessionChecker, (req, res) => {
  res.redirect('/login');
});

// route for user Login
app.route('/login')
  .get(sessionChecker, (req, res) => {
    res.sendFile(__dirname + '/views/public/signin.html');
  })
  .post((req, res) => {
    var username = req.body.username,
      password = req.body.password;
      con.query('SELECT username, password FROM users WHERE username = "' + username + '" and password = "' + password + '"', (err, result) => {
        if (err) throw err;
        if (result.length > 0) {
          req.session.user = result[0];
          res.redirect('/home');
        } else {
          res.redirect('/login');
        }
      })
  });

// route for user logout
app.get('/logout', (req, res) => {
  if (req.session.user && req.cookies.user_sid) {
    res.clearCookie('user_sid');
    res.redirect('/');
  } else {
    res.redirect('/login');
  }
});

// route for home
app.get('/home', (req, res) => {
  if (req.session.user && req.cookies.user_sid) {
    res.sendFile(__dirname + '/views/public/home.html');
  } else {
    res.redirect('/login');
  }
});

// route for dashboard
app.get('/dashboard', (req, res) => {
  if (req.session.user && req.cookies.user_sid) {
    res.sendFile(__dirname + '/views/public/dashboard.html');
  } else {
    res.redirect('/login');
  }
});

// route for preference
app.route('/preference')
  .get((req, res) => {
    if (req.session.user && req.cookies.user_sid) {
      res.sendFile(__dirname + '/views/public/preference.html');
    } else {
      res.redirect('/login');
    }
  })
  .post((req, result) => {
    var form = req.body.form;
    if (form == 'username') { //Change username
      if (req.body.current === req.session.user['password']) {
        con.query('UPDATE users SET username = "' + req.body.username + '" WHERE id = "1"', (err, res) => {
          if (err) throw err;
          req.session.user['username'] = req.body.username;
          result.redirect('/preference');
        });
      } else {
        result.redirect('/preference'); //Incorrect password
      }
    } else if (form == 'password') { //Change password
      if (req.body.current === req.session.user['password']) {
        con.query('UPDATE users SET password = "' + req.body.newpassword + '" WHERE id = "1"', (err, res) => {
          if (err) throw err;
          req.session.user['password'] = req.body.newpassword;
          result.redirect('/preference');
        });
      } else {
        result.redirect('/preference'); //Incorrect password
      }
    } else if (form == 'cost') { //Change cost
      con.query('UPDATE unit SET usd_per_kw = ' + req.body.cost + ' WHERE unID = "1"', (err, res) => {
        if (err) throw err;
        result.redirect('/preference');
      });
    } else if (form == 'clear') { //Erase log data
      con.query('DELETE FROM event_log WHERE isCal = "1"', (err, res) => {
        if (err) throw err;
        con.query('UPDATE event_log SET visible = "0" WHERE eID > 0', (err, res) => {
          result.redirect('/preference');
        });
      })
    }
  });

//Get data to display in Preference page
app.get('/pref/log', (req, result) => {
  con.query('SELECT currency FROM unit', (err, res) => {
    if (err) throw err;
    var currency = res[0].currency;
    var isUSD = true;
    if (!(currency == "USD")) {
      isUSD = false;
    }
    var logData = [];
    var stats = [];
    // Get 16 most recent event logs (on/off events)
    con.query('SELECT * FROM event_log WHERE visible <> "0" ORDER BY eID DESC LIMIT 16', (err, res) => {
      if (err) throw err;
      for (i = 0; i < res.length; i++) {
        var dateTime = dateFormat(res[i].dateTime, "yyyy-mm-dd h:MM:ss TT");
        var type = '';
        var comID = res[i].comID;
        if (comID == '1') {
          type = ' - Ground floor: Light';
        } else if (comID == '2') {
          type = ' - Ground floor: Fan';
        } else if (comID = '3') {
          type = ' - First floor: Light';
        }
        var display = res[i].event + type;
        var row = { date: dateTime, event: display };
        logData.push(row);
      }
      //Get 7 most recent daily usage
      con.query('SELECT * FROM (SELECT usgID, date, watt, usd_total total FROM energy_usage ORDER BY usgID DESC LIMIT 7) sub ORDER BY usgID ASC', (err, res) => {
        if (err) throw err;
        for (i = 0; i < res.length; i++) {
          var day = dateFormat(res[i].date, "mm-dd");
          var row = { date: day, watt: res[i].watt, total: res[i].total };
          stats.push(row);
        }
        //Get monthly usage
        con.query('select year(date) y, month(date) m , sum(watt) watt, sum(usd_total) cost from energy_usage group by month(date)', (err, res) => {
          if (err) throw err;
          var monthly = [];
          for (i = 0; i < res.length; i++) {
            var row = { y: res[i].y, m: res[i].m, watt: res[i].watt, cost: res[i].cost };
            monthly.push(row);
          }
          result.json({ message: logData, stat: stats, curr: isUSD, month: monthly });
        })
      });
    });
  });
});

// Get currency, cost per kwh, and wph of each component
app.get('/pref/data', function (req, result, next) {
  var dat;
  con.query('SELECT * FROM unit', (err, res) => {
    if (err) throw err;
    dat = res[0];
    con.query('SELECT * FROM component', (err, res2) => {
      if (err) throw err;
      var comData = [];
      for (i = 0; i < res2.length; i++) {
        comData.push(res2[i]);
      }
      result.json({ data: dat, comDat: comData, user: req.session.user });
    });
  });
});

//allow user to download log data
app.get('/logdownload', function (req, res) {
  con.query('SELECT * FROM event_log', (err, result) =>{
    if (err) throw err;
    var data = "componentID: 1: Ground floor light, 2: Fan, 3: First floor light\n";
    for(i = 0; i < result.length; i++) {
      var row = "eID: " + result[i].eID + ", dataTime: " + dateFormat(result[i].dateTime, "yyyy-mm-dd h:MM:ss TT") + ", event: " + result[i].event + ", componentID: " + result[i].comID;
      data = data + row + "\n";
    }
    var file = __dirname + '/views/public/logData.txt';
    fs.writeFile(file, data, function (err) {
      if (err) throw err;
      res.download(file);
    }); 
  })
})

/* ------ Control Lights and Fan --------- */

/* ----- Ground floor lights  ----- */

//On
app.post('/light/g/on', (req, res) => {
  // Record light status to database
  con.query('UPDATE unit SET gl = 1 WHERE unID = "1"', (err, result) => {
    if (err) throw err;
  })
  controlLight('light/g/command', 1, "1", "On", res);
});

//Off
app.post('/light/g/off', (req, res) => {
  con.query('UPDATE unit SET gl = 0 WHERE unID = "1"', (err, result) => {
    if (err) throw err;
  })
  controlLight('light/g/command', 1, "0", "Off", res);
});

/* ----- First floor lights  ----- */

//On
app.post('/light/f/on', (req, res) => {
  con.query('UPDATE unit SET fl = 1 WHERE unID = "1"', (err, result) => {
    if (err) throw err;
  })
  controlLight('light/f/command', 3, "1", "On", res);
});

//Off
app.post('/light/f/off', (req, res) => {
  con.query('UPDATE unit SET fl = 0 WHERE unID = "1"', (err, result) => {
    if (err) throw err;
  })
  controlLight('light/f/command', 3, "0", "Off", res);
});

/* ----- Fan ----- */

//On
app.post('/fan/on', (req, res) => {
  con.query('UPDATE unit SET fan = 1 WHERE unID = "1"', (err, result) => {
    if (err) throw err;
  })
  controlLight('fan/command', 2, "1", "On", res);
});

//Off
app.post('/fan/off', (req, res) => {
  con.query('UPDATE unit SET fan = 0 WHERE unID = "1"', (err, result) => {
    if (err) throw err;
  })
  controlLight('fan/command', 2, "0", "Off", res);
});

/* ----- Switch currency ----- */

// To Riel
app.post('/pref/swCurr/USD', (req, result) => {
  con.query("UPDATE unit SET currency = 'Riel' WHERE unID = '1'", (err, res) => {
    if (err) throw err;
    result.sendStatus(201);
    result.end;
  });
});

// To USD
app.post('/pref/swCurr/Riel', (req, result) => {
  con.query("UPDATE unit SET currency = 'USD' WHERE unID = '1'", (err, res) => {
    if (err) throw err;
    result.sendStatus(201);
    result.end;
  });
});

// route for handling 404 requests(unavailable routes)
app.use(function (req, res, next) {
  res.status(404).send("Sorry can't find that!")
});

/* ---------- Functions ----------- */

var today = dateFormat(new Date(), "yyyy-mm-dd");
con.query('SELECT * FROM energy_usage WHERE date = "' + today + '"', function (err, result, fields) {
  if (err) throw err;
  if (result.length > 0) {
    isTodayLogged = true;
  }
});

// When usage for an on/off event has been calculated.
function updateCal(idList) {
  con.query('UPDATE event_log SET isCal = 1 WHERE eID = "' + idList[0] + '" or eID = "' + idList[1] + '"', (err, res) => {
    if (err) throw err;
    return true;
  })
}

// Add usage data
function updateUsage(watt, cost, ids) {
  con.query('UPDATE energy_usage SET watt = watt + ' + watt + ' , usd_total = usd_total + ' + cost + ' WHERE usgID = "' + todayUsgID + '"', (err, res) => {
    if (err) throw err;
    if(ids.length > 0 ) {
      updateCal(ids);
    }
    return true;
  })
}

function calculateUsage(comID) {
  var day = dateFormat(new Date(), "yyyy-mm-dd");
  var minute;
  var cost;
  var ids = [];
  con.query('SELECT eID, dateTime, comID, event FROM event_log WHERE dateTime BETWEEN "' + day + ' 00:00:00" and "' + day + ' 23:59:59" and isCal <> 1 and comID = "' + comID + '"', function (err, result, fields) {
    if (err) throw err;
    if (result.length > 0) {
      ids.push(result[0].eID);
      ids.push(result[1].eID);
      var firstdate = result[0].dateTime,
        secDate = result[1].dateTime,
        timeDifference = Math.abs(secDate.getTime() - firstdate.getTime());
      minute = Math.ceil(Math.abs((timeDifference / 1000) / 60));
      con.query('SELECT usd_per_kw cost FROM unit', (err, res) => {
        if (err) throw err;
        cost = res[0].cost;
        con.query('SELECT wph FROM component WHERE comID = "' + comID + '"', (err, res) => {
          var totalWatt = res[0].wph * (minute / 60);
          var totalCost = cost * (totalWatt / 1000);
          totalWatt = totalWatt.toFixed(4);
          totalCost = totalCost.toFixed(4);
          // If no usage for today has been recored, create a new row 
          // If usage for today is already calculated once, just add new usage data to the corresponding row
          if (isTodayLogged) {
            if (todayUsgID != -1) {
              updateUsage(totalWatt, totalCost, ids);
            } else {
              var now = dateFormat(new Date(), "yyyy-mm-dd");
              con.query('SELECT usgID FROM energy_usage WHERE date = "' + now + '"', (err, res) => {
                if (err) throw err;
                todayUsgID = res[0].usgID;
                updateUsage(totalWatt, totalCost, ids);
              })
            }
          } else {
            var record = { usgID: '', date: day, watt: totalWatt, usd_per_kw: cost, usd_total: totalCost };
            con.query('INSERT INTO energy_usage SET ?', record, (err, res) => {
              if (err) throw err;
              todayUsgID = res.insertId;
              updateCal(ids);
              return true;
            });
          }
        })
      })
    }
  });
}

//Record turn on/off
function recordEvent(eventMsg, comID) {
  var d = new Date();
  var event = { eID: '', dateTime: d, event: eventMsg, isCal: 0, comID: comID, visible: 1 };
  con.query('INSERT INTO event_log SET ?', event, (err, res) => {
    if (err) throw err;
    if (eventMsg == "Off") {
      calculateUsage(comID);
    }
    return true;
  });
}

//Control Light - Publish control message to mqtt
function controlLight(inputTopic, comID, command, event, res) {
  client = mqtt.connect('http://127.0.0.1');
  client.on('connect', () => {
    client.publish(inputTopic, command);
    recordEvent(event, comID);
    res.sendStatus(201);
    res.end();
  });
}

// var job = schedule.scheduleJob('55 23 * * *', () => {
//   calculateDailyUsage();
// });

