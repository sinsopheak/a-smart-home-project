-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2018 at 02:33 AM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_smart_home`
--

-- --------------------------------------------------------

--
-- Table structure for table `component`
--

CREATE TABLE `component` (
  `comID` int(11) NOT NULL,
  `title` varchar(10) NOT NULL,
  `floor` int(11) NOT NULL,
  `num` int(11) NOT NULL,
  `wph` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `component`
--

INSERT INTO `component` (`comID`, `title`, `floor`, `num`, `wph`) VALUES
(1, 'light', 0, 4, 3),
(2, 'fan', 0, 1, 5),
(3, 'light', 1, 4, 3);

-- --------------------------------------------------------

--
-- Table structure for table `energy_usage`
--

CREATE TABLE `energy_usage` (
  `usgID` int(11) NOT NULL,
  `date` date NOT NULL,
  `watt` float NOT NULL,
  `usd_per_kw` float NOT NULL,
  `usd_total` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `energy_usage`
--

INSERT INTO `energy_usage` (`usgID`, `date`, `watt`, `usd_per_kw`, `usd_total`) VALUES
(6, '2018-06-29', 20.1667, 0.1875, 0.0038),
(7, '2018-06-30', 20.1667, 0.1875, 0.0038),
(8, '2018-07-01', 30, 0.1875, 0.0056),
(9, '2018-07-02', 30, 0.1875, 0.0056),
(10, '2018-07-03', 30, 0.1875, 0.0056),
(11, '2018-07-04', 20.1667, 0.1875, 0.0038),
(12, '2018-07-05', 30, 0.1875, 0.0056),
(19, '2018-07-10', 21.6, 0.1875, 0.004),
(20, '2018-07-11', 5.4662, 0.187, 0.0004);

-- --------------------------------------------------------

--
-- Table structure for table `event_log`
--

CREATE TABLE `event_log` (
  `eID` int(11) NOT NULL,
  `dateTime` datetime NOT NULL,
  `event` varchar(10) NOT NULL,
  `isCal` int(11) NOT NULL DEFAULT '0',
  `comID` int(11) NOT NULL DEFAULT '1',
  `visible` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `event_log`
--

INSERT INTO `event_log` (`eID`, `dateTime`, `event`, `isCal`, `comID`, `visible`) VALUES
(244, '2018-07-11 10:11:45', 'On', 1, 3, 0),
(245, '2018-07-11 10:11:47', 'Off', 1, 3, 0),
(246, '2018-07-11 10:16:47', 'On', 0, 3, 1),
(247, '2018-07-11 10:16:48', 'On', 1, 1, 1),
(248, '2018-07-11 10:22:20', 'Off', 0, 3, 1),
(249, '2018-07-11 10:22:23', 'Off', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `unID` int(11) NOT NULL,
  `currency` varchar(10) NOT NULL DEFAULT 'USD',
  `usd_per_kw` float NOT NULL DEFAULT '0.1875',
  `gl` int(11) NOT NULL DEFAULT '0',
  `fan` int(11) NOT NULL DEFAULT '0',
  `fl` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`unID`, `currency`, `usd_per_kw`, `gl`, `fan`, `fl`) VALUES
(1, 'USD', 0.2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL DEFAULT 'admin',
  `password` varchar(50) NOT NULL DEFAULT 'admin'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`) VALUES
(1, 'test', '1234');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `component`
--
ALTER TABLE `component`
  ADD PRIMARY KEY (`comID`);

--
-- Indexes for table `energy_usage`
--
ALTER TABLE `energy_usage`
  ADD PRIMARY KEY (`usgID`),
  ADD UNIQUE KEY `date` (`date`);

--
-- Indexes for table `event_log`
--
ALTER TABLE `event_log`
  ADD PRIMARY KEY (`eID`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`unID`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `component`
--
ALTER TABLE `component`
  MODIFY `comID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `energy_usage`
--
ALTER TABLE `energy_usage`
  MODIFY `usgID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `event_log`
--
ALTER TABLE `event_log`
  MODIFY `eID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=250;
--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `unID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
